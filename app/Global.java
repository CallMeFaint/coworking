import play.Application;
import play.GlobalSettings;
import play.libs.Akka;
import scala.concurrent.duration.Duration;
import timers.ExpiredOrdersRemover;

import java.util.concurrent.TimeUnit;

/**
 * Created by Damian on 2014-11-14.
 */
public class Global extends GlobalSettings {
    private Runnable expiredOrdersTimer;

    @Override
    public void onStart(Application app) {
        System.out.println("Tworzenie timerow");
        expiredOrdersTimer = new ExpiredOrdersRemover();

        Akka.system().scheduler().schedule(
                Duration.create(10, TimeUnit.SECONDS),
                Duration.create(12, TimeUnit.HOURS),
                expiredOrdersTimer,
                Akka.system().dispatcher()
        );
    }
}
