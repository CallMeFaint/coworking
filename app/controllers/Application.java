package controllers;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import models.office.Office;
import models.office.Reservation;
import org.joda.time.DateTime;
import play.*;
import play.cache.Cache;
import play.data.format.Formats;
import play.mvc.*;

import tools.KeyCodes;
import tools.UniversalIdGenerator;
import views.html.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Application extends Controller {

    public static Result index() {

        return ok(enterpage.render());
    }

    public static Result admin() {
        List<Office> offices = Office.list();
        return ok(views.html.admin.officesList.render(offices));
    }

    public static Result reservations() {
        List<Reservation> reservations = Reservation.getAll();
        return ok(views.html.admin.reservationsList.render(reservations));
    }

    public static Result captcha() {
        DefaultKaptcha captcha = new DefaultKaptcha();
        captcha.setConfig(new Config(new Properties()));
        String text = captcha.createText();

        String uid = UniversalIdGenerator.generate();
        flash(KeyCodes.FLASH_CAPTCHA, uid);
        Cache.set(uid, text);

        Logger.debug("Captcha:" + text);//U can put the text in cache.
        BufferedImage img = captcha.createImage(text);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, "jpg", baos);
            baos.flush();
        } catch (IOException e) {
            Logger.debug(e.getMessage());
        }
        return ok(baos.toByteArray()).as("image/jpg");
    }
}
