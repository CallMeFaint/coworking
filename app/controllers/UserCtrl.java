package controllers;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import models.user.User;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import security.Permission;
import tools.KeyCodes;

public class UserCtrl extends Controller {
    public static final String URL_REGISTRATION_INDEX = "/user/registration";
    public static final String URL_LOGIN_INDEX = "/user/login";
    private static final String URL_HOME = "/";

    /**
     * @return Zwraca formularz z logowaniem użytkownika.
     */
    public static Result login() {
        return ok(views.html.user.login.render());
    }

    /**
     * @return Zwraca formularz z rejestracją użytkownika.
     */
    public static Result registration() {
        return ok(views.html.user.registration.render());
    }

    /**
     * Logowanie użytkownika
     * @return Po udanej lub nieudanej próbie następuje przekierowanie do strony głównej.
     */
    public static Result signIn() {
        DynamicForm form = Form.form().bindFromRequest();
        String email = form.get("email");
        String password = form.get("password");
        User candidate = new User(email, password);
        User dbUser = User.findByEmail(candidate.getEmail());

        if (dbUser != null && dbUser.equals(candidate)) {
            session(KeyCodes.SESSION_USER, dbUser.getEmail());
            if (dbUser.getPermissionsAsList().contains(Permission.ADMIN.getValue()))
                session(KeyCodes.SESSION_ADMIN, Permission.ADMIN.toString());
            return redirect(URL_HOME);
        }
        else {
            flash(KeyCodes.FLASH_ERROR, Messages.get("login.error.user.invalid"));
            return redirect(URL_LOGIN_INDEX);
        }


    }

    /**
     * Wylogowanie użytkownika.
     * @return Po wylogowaniu użytkownika zawsze nastąpi przekierowanie do strony głównej.
     */
    public static Result signOut() {
        session().clear();
        return redirect(URL_HOME);
    }

    /**
     * Rejestracja użytkownika.
     * @return W przypadku rejestracji zakończonej sukcesem użytkownik zostanie przekierowany na stronę główną.
     * W przeciwnym wypadku zostaje na stronie z formularzem.
     */
    public static Result signUp() {
        DynamicForm form = Form.form().bindFromRequest();
        String email = form.get("email");
        String password = form.get("password");
        String repeatedPassword = form.get("repeatedPassword");
        String firstName = form.get("firstName");
        String lastName = form.get("lastName");
        String captcha = form.get("captcha");

        String captchaKey = (String) flash().get(KeyCodes.FLASH_CAPTCHA);
        String generatedCaptcha = (String) Cache.get(captchaKey);

        if (captcha == null || captcha.isEmpty() || !captcha.equals(generatedCaptcha)) {
            flash(KeyCodes.FLASH_ERROR, Messages.get("registration.error.captcha"));
            return redirect(URL_REGISTRATION_INDEX);
        }

        if (email == null || email.isEmpty()) {
            flash(KeyCodes.FLASH_ERROR, Messages.get("registration.error.email.empty"));
            return redirect(URL_REGISTRATION_INDEX);
        }

        if (password != null && password.equals(repeatedPassword)) {
            User user = new User(email, password, firstName, lastName);
            try {
                user.save();
            } catch (Exception e) {
                flash(KeyCodes.FLASH_ERROR, Messages.get("registration.error.user.exists"));
                return redirect(URL_REGISTRATION_INDEX);
            }

            flash(KeyCodes.FLASH_SUCCESS, Messages.get("registration.success"));
            return redirect(URL_HOME);
        }
        else {
            flash(KeyCodes.FLASH_ERROR, Messages.get("registration.error.passwords.not.the.same"));
            return redirect(URL_REGISTRATION_INDEX);
        }
    }
}