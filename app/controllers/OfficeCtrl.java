package controllers;

import models.office.Reservation;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.FileCopyUtils;
import play.libs.Json;
import play.mvc.Http;
import security.Authorized;
import security.Permission;
import tools.KeyCodes;
import models.office.Address;
import models.office.Office;
import models.office.Workplace;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Damian on 2014-10-25.
 */
public class OfficeCtrl extends Controller{
    private static final String IMAGES_STORAGE = "E:\\TMP\\";
    public static final String URL_OFFICE_LIST = "/office/list";
    public static final String URL_OFFICE_CREATE = "/office/create";

    public static Result list() {
        List<Office> offices = Office.list();

        return ok(views.html.office.list.render(offices));
    }

    public static Result getOfficeImage(String path) {
        return ok(new File(path));
    }

    public static Result edition(final long id){
        Office office = Office.findById(id);
        if (office == null)
            return badRequest();
        else
            return ok(views.html.office.edit.render(office));
    }

    public static Result create() {
        return ok(views.html.office.create.render());
    }

    public static Result details(final long id) {
        Office office = Office.findById(id);
        if (office != null)
            return ok(views.html.office.details.render(office));
        return badRequest();
    }

    public static Result search() {
        DynamicForm form = Form.form().bindFromRequest();
        String input = form.get("input");
        Set<Office> offices = new HashSet<>();
        offices.addAll(Office.findByNameLike(input));
        offices.addAll(Office.findByCity(input));
        return ok(views.html.office.list.render(offices));
    }


    public static Result getReservationsFromDate(final long officeId, final String date) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime dateTime = formatter.parseDateTime(date);
            LocalDate startDate = dateTime.toLocalDate();
            List<Properties> output = new ArrayList<>();
            List<Workplace> workplaces = Office.findById(officeId).getWorkplaces();
            for (Workplace w : workplaces) {
                List<Reservation> reservationsAfter = Reservation.findByDate(w.getId(), startDate.toDate(), startDate.plus(Period.days(30)).toDate());
                List<Reservation> reservationsBefore = Reservation.findByDate(w.getId(), startDate.minus(Period.days(30)).toDate(), startDate.toDate());
                Set<Reservation> reservations = new HashSet<>();
                reservations.addAll(reservationsAfter);
                reservations.addAll(reservationsBefore);

                Properties workplacesAsProperties = new Properties();
                workplacesAsProperties.put("reservations",convertReservationsToProperties(reservations));
                workplacesAsProperties.put("id", w.getId());
                output.add(workplacesAsProperties);
            }
            return ok(Json.toJson(output));
        } catch (Exception e) {
            return badRequest();
        }
    }

    private static List<Properties> convertReservationsToProperties(Set<Reservation> reservations) {
        List<Properties> out = new ArrayList<>();
        for (Reservation reservation : reservations) {
            Properties p = new Properties();
            p.setProperty("id", String.valueOf(reservation.getId()));
            p.setProperty("owner", reservation.getOwner().toString());
            p.setProperty("state", String.valueOf(reservation.getState()));
            out.add(p);
        }
        return out;
    }

    /**
     * Utworzenie nowego biura co-workingowego.
     * @return status 400 w przypadku gdy operacja się nie powiodła; przekiwerowanie do listy biur w przeciwnym wypadku.
     */
    public static Result add() {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Map<String, String[]> data = body.asFormUrlEncoded();
        String name =  data.get("name")[0];
        String description = data.get("description")[0];
        String city = data.get("city")[0];
        String street = data.get("street")[0];
        String prize = data.get("prize")[0];
        // w wersji demo na sztywno 4: String workplaces = data.get("workplaces")[0];
        String workplaces = "4";
        Http.MultipartFormData.FilePart filePart = body.getFile("smallPicture");

        Office office = createOffice(name, description, city, street, workplaces, prize);

        if (filePart != null) {
            File file = filePart.getFile();
            String path = IMAGES_STORAGE + filePart.getFilename();
            String errorDescription = uploadFile(file, path);
            if (errorDescription != null)
                return redirectWithError(URL_OFFICE_CREATE, errorDescription);
            else
            return saveOffice(office, path);
        } else {
            return saveOffice(office, null);
        }
    }

    public static Result edit(long id) {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Map<String, String[]> data = body.asFormUrlEncoded();
        String name =  data.get("name")[0];
        String description = data.get("description")[0];
        String city = data.get("city")[0];
        String street = data.get("street")[0];
        String prize = data.get("prize")[0];
        //String workplaces = data.get("workplaces")[0];
        String workplaces = "4";
        Http.MultipartFormData.FilePart filePart = body.getFile("smallPicture");

        Office office = Office.findById(id);
        office.setName(name);
        office.setDescription(description);
        office.getAddress().setCity(city);
        office.getAddress().setStreet(street);
        office.setPrize(new BigDecimal(prize));

        int workplacesDifference = office.getWorkplaces().size() - Integer.valueOf(workplaces);
        if (workplacesDifference > 0) {
            for (int i = office.getWorkplaces().size() - 1; i >= workplacesDifference; i--)
                office.getWorkplaces().get(i).delete();
        } else if (workplacesDifference < 0 ) {
            for (int i = 0; i < Math.abs(workplacesDifference); i++)
                office.getWorkplaces().add(new Workplace());
        }

        if (filePart != null) {
            File file = filePart.getFile();
            String path = IMAGES_STORAGE + filePart.getFilename();
            String errorDescription = uploadFile(file, path);
            if (errorDescription != null)
                return redirectWithError(URL_OFFICE_CREATE, errorDescription);
            else
                return updateOffice(office, path);
        } else {
            return updateOffice(office, null);
        }
    }

    public static Result remove(final Long id) {
        try {
            Office office = Office.findById(id);
            office.delete();
            flash(KeyCodes.FLASH_SUCCESS, Messages.get("office.message.remove.success"));
            return redirect(URL_OFFICE_LIST);
        } catch (Exception e) {
            return redirectWithError(URL_OFFICE_LIST, Messages.get("office.message.remove.failure"));
        }
    }

    private static Result updateOffice(Office office, String picturePath) {
        if (office != null) {
            if (picturePath != null)
                office.setPicturePath(picturePath);
            office.update();
            flash(KeyCodes.FLASH_SUCCESS, Messages.get("office.update.success"));
            return redirect(URL_OFFICE_LIST);
        }
        else {
            return redirectWithError(URL_OFFICE_CREATE, Messages.get("office.create.error.unrecognized"));
        }
    }

    private static Result saveOffice(Office office, String picturePath) {
        if (office != null) {
            office.setPicturePath(picturePath);
            office.save();
            flash(KeyCodes.FLASH_SUCCESS, Messages.get("office.create.success"));
            return redirect(URL_OFFICE_LIST);
        }
        else {
            return redirectWithError(URL_OFFICE_CREATE, Messages.get("office.create.error.unrecognized"));
        }
    }

    private static String uploadFile(File file, String path) {
        try {
            FileCopyUtils.copy(file, new File(path));
        } catch (IOException e) {
            return Messages.get("office.create.error.image.upload.failure");
        } catch (Exception e) {
           return Messages.get("office.create.error.unrecognized");
        }
        return null;
    }

    /**
     * Metoda tworzy nowe biuro na podstawie przekazanych parametrow.
     * @param name  nazwa biura.
     * @param description  opis biura,
     * @param city  miasto w którym znajduje się biurom
     * @param street  ulica na jakiej jest polożone biuro,
     * @param workplaces  ilość (jako String) przewidzianych miesc,
     * @param prize cena za jedno stanowisko.
     * @return nowy obiekt biura, lub null w przypadku wystąpienia błędu.
     */
    private static Office createOffice(String name, String description, String city, String street, String workplaces, String prize) {
        int amountOfWorkplaces;
        Office office = new Office();

        if (name.isEmpty() || city.isEmpty() || street.isEmpty()) {
            flash(KeyCodes.FLASH_ERROR,  Messages.get("office.create.error.empty.fields"));
            return null;
        }

        try {
            if (!workplaces.isEmpty()) {
                amountOfWorkplaces = Integer.parseInt(workplaces);
                List<Workplace> officeWorkplaces = new ArrayList<>();
                for (int i = 0 ; i < amountOfWorkplaces; i++)
                    officeWorkplaces.add(new Workplace());
                office.setWorkplaces(officeWorkplaces);
            }
            office.setPrize(new BigDecimal(prize));
        } catch (NumberFormatException exception) {
            flash(KeyCodes.FLASH_ERROR,  Messages.get("office.create.error.empty.fields"));
            return null;
        }

        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        office.setName(name);
        office.setDescription(description);
        office.setAddress(address);

        return office;
    }

    private static Result redirectWithError(String url, String message) {
        flash(KeyCodes.FLASH_ERROR, message);
        return redirect(url);
    }
}
