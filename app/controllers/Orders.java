package controllers;

import models.office.Office;
import models.office.Reservation;
import models.office.Workplace;
import models.user.User;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import security.Authorized;
import security.Permission;
import tools.KeyCodes;

import java.util.*;

/**
 * Created by Damian on 2014-11-08.
 */
public class Orders extends Controller{


    @Authorized(permissions={Permission.AUTHORIZED})
    public static Result make(Long officeId) {
        DynamicForm form = Form.form().bindFromRequest();
        String workplaceId = form.get("workplaceId");
        String startDate = form.get("startDate");
        try {
            User user = User.findByEmail(session().get(KeyCodes.SESSION_USER));
            Office office = Office.findById(officeId);
            Workplace workplace = office.getWorkplaceById(Long.valueOf(workplaceId));
            LocalDate currentDate = new LocalDate();
            LocalDate beginningDate = createBeginningDate(startDate);
            LocalDate endDate = currentDate.plus(Period.days(Reservation.MAX_DAYS_BOOKED));

            if (currentDate.isAfter(beginningDate)) {
                flash(KeyCodes.FLASH_ERROR, Messages.get("reservations.beggining.date.before.current"));
                return redirect(OfficeCtrl.URL_OFFICE_LIST);
            }

            Reservation reservation = new Reservation();
            reservation.setCreationDate(currentDate.toDate());
            reservation.setBeginningDate(beginningDate.toDate());
            reservation.setExpiryDate(endDate.toDate());
            reservation.setOwner(user);
            reservation.setState(Reservation.BOOKED);
            reservation.setWorkplace(workplace);



            if (workplace.getReservations() == null || workplace.getReservations().size() == 0) {
                List<Reservation> reservations = new ArrayList<>();
                reservations.add(reservation);
                workplace.setReservations(reservations);
            } else {
                workplace.getReservations().add(reservation);
            }

            workplace.update();
        } catch (Exception e){
            e.printStackTrace();
            flash(KeyCodes.FLASH_ERROR, Messages.get("reservations.unknown.error"));
            return redirect(OfficeCtrl.URL_OFFICE_LIST);
        }

        flash(KeyCodes.FLASH_SUCCESS, Messages.get("office.order.make.success"));
        return redirect(OfficeCtrl.URL_OFFICE_LIST);
    }

    private static LocalDate createBeginningDate(final String date){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime dateTime = formatter.parseDateTime(date);
        return dateTime.toLocalDate();
    }

    @Authorized(permissions={Permission.AUTHORIZED})
    public static Result getAll() {
        String email = session().get(KeyCodes.SESSION_USER);
        List<Reservation> reservations = Reservation.getByUser(email);
        return ok(views.html.order.index.render(reservations));
    }

    @Authorized(permissions={Permission.AUTHORIZED})
    public static Result cancel(final long id) {
        Reservation reservation = Reservation.findById(id);
        reservation.delete();
        flash(KeyCodes.FLASH_SUCCESS, Messages.get("office.order.cancel.success"));
        return redirect("/user/orders");
    }

    @Authorized(permissions={Permission.AUTHORIZED})
    public static Result confirm(final long id) {
        try {
            Reservation reservation = Reservation.findById(id);
            reservation.setState(Reservation.PROCESSING);
            reservation.update();
            flash(KeyCodes.FLASH_SUCCESS, Messages.get("reservation.confirm.message"));
        } catch (Exception e) {
            flash(KeyCodes.FLASH_ERROR, Messages.get("office.order.confirm.error"));
        }
        return redirect("/user/orders");
    }

    @Authorized(permissions={Permission.ADMIN})
    public static Result accept(final long id) {
        try {
            Reservation reservation = Reservation.findById(id);
            reservation.setState(Reservation.SOLD);
            LocalDate beginningDate = new LocalDate(reservation.getBeginningDate());
            reservation.setExpiryDate(beginningDate.plusDays(Reservation.MAX_DAYS_SOLD).toDate());
            reservation.update();
        } catch (Exception e) {
            flash(KeyCodes.FLASH_ERROR, Messages.get("office.order.confirm.error"));
        }
        return redirect("/admin/reservations");
    }
}
