package tools;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * Created by Damian on 2015-01-04.
 */
public class UniversalIdGenerator {
    public static String generate() {
        return UUID.randomUUID().toString();
    }

    private UniversalIdGenerator() {}
}
