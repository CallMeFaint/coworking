package tools;

/**
 * Created by Damian on 2014-10-25.
 */
public final class KeyCodes {
    public static final String FLASH_ERROR = "ERROR";
    public static final String FLASH_SUCCESS = "SUCCESS";
    public static final String SESSION_USER = "USER";
    public static final String SESSION_ADMIN = "ADMIN";
    public static final String FLASH_CAPTCHA = "CAPTCHA";
    private KeyCodes() {}
}
