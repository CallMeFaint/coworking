package security;

import controllers.UserCtrl;
import models.user.User;
import play.i18n.Messages;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import tools.KeyCodes;

import static play.mvc.Controller.flash;

/**
 * Created by Damian on 2014-11-01.
 */
public class Authorization extends Action<Authorized> {

    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        if (!ctx.session().containsKey(KeyCodes.SESSION_USER))
            return F.Promise.pure(redirect(UserCtrl.URL_LOGIN_INDEX));
        else if (configuration.permissions().length == 1
                && configuration.permissions()[0].equals(Permission.AUTHORIZED)) {
            return delegate.call(ctx);
        }
        else{
            String email = ctx.session().get(KeyCodes.SESSION_USER);
            User user = User.findByEmail(email);
            if(!hasPermissions(configuration.permissions(), user)) {
                flash(KeyCodes.FLASH_ERROR, Messages.get("login.error.permissions"));
                return F.Promise.pure(redirect(UserCtrl.URL_LOGIN_INDEX));
            } else {
                return delegate.call(ctx);
            }
         }
    }

    private boolean hasPermissions(Permission[] permissions, User user) {
        if (user == null)
            return false;

        for (Permission p : permissions)
            if (!user.getPermissionsAsList().contains(p.getValue()))
                return false;
        return true;
    }
}
