package security;

/**
 * Created by Damian on 2014-11-02.
 */
public enum Permission {
    UNAUTHORIZED(0),
    AUTHORIZED(1),
    ADMIN(99);

    private final int value;

    Permission(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
