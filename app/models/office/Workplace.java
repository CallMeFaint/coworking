package models.office;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Damian on 2014-10-25.
 */
@Entity

public class Workplace extends Model{
    @Id
    private long id;
    @ManyToOne
    @JoinColumn(name="office_id")
    private Office office;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Reservation> reservations;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
