package models.office;

import play.db.ebean.Model;

import javax.persistence.*;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.util.Date;
import java.util.List;

/**
 * Created by Damian on 2014-10-25.
 */
@Entity
public class Office extends Model {
    @Id
    private long id;

    private String picturePath;

    @Column(unique=true)
    private String name;

    private String description;

    @OneToOne(cascade=CascadeType.ALL)
    private Address address;

    @OneToMany(cascade=CascadeType.ALL)
    private List<Workplace> workplaces;

    private BigDecimal prize;

    private static Finder<String,Office> find = new Finder<String,Office>(
            String.class, Office.class
    );

    public Office() {}

    public Office(String name, String description, Address address, String picturePath, List<Workplace> workplaces, BigDecimal prize) {
        this.picturePath = picturePath;
        this.name = name;
        this.description = description;
        this.address = address;
        this.workplaces = workplaces;
        this.prize = prize;
    }

    public static List<Office> list() {
        return find.all();
    }

    public static Office findByName(final String name) {
        return find.where().eq("name", name).findUnique();
    }

    public static List<Office> findByCity(final String city) {
        return find.fetch("address").where().like("address.city", "%" + city + "%").findList();
    }

    public static List<Office> findByNameLike(final String name) {
        return find.where().like("name", "%" + name + "%").findList();
    }

    public static Office findById(final long id) {
        return find.where().eq("id", id).findUnique();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Workplace> getWorkplaces() {
        return workplaces;
    }

    public void setWorkplaces(List<Workplace> workplaces) {
        this.workplaces = workplaces;
    }

    public int getWorkplacesAmount() {
        return getWorkplaces().size();
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public File getPictureFromPath() {
        return new File(picturePath);
    }

    public BigDecimal getPrize() {
        return prize;
    }

    public void setPrize(BigDecimal prize) {
        this.prize = prize;
    }

    public Workplace getWorkplaceById(final long id) {
        for (Workplace workplace : workplaces)
            if (workplace.getId() == id)
                return workplace;
        return null;
    }

    @Override
    public int hashCode() {
        return name.hashCode() * address.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

}
