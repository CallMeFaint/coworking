package models.office;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Damian on 2014-10-25.
 */
@Entity
public class Address extends Model {
    @Id
    private long id;
    private String city;
    private String street;

    public Address() {
    }

    public Address(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Address) {
            Address otherAddress = (Address) other;
            return city.equals(otherAddress.city) && street.equals(otherAddress.street);
        }
        return false;
    }

    @Override
    public String toString() {
        return getCity() + ", " + getStreet();
    }

    @Override
    public int hashCode() {
        return (city + street).hashCode();
    }
}
