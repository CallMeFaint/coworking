package models.office;

import models.user.User;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Damian on 2014-10-25.
 */
@Entity
public class Reservation extends Model{
    public static final int FREE = 0;
    public static final int SOLD = 1;
    public static final int BOOKED = 2;
    public static final int PROCESSING = 3;
    public static final int MAX_DAYS_BOOKED = 7;
    public static final int MAX_DAYS_SOLD= 31;
    @Id
    private long id;

    /**
     * Data utworzenia zamowienia
     */
    private Date creationDate;

    /**
     * Data od ktorej zamowienie jest wazne
     */
    private Date beginningDate;

    /**
     * Data konca zamowienia
     */
    private Date expiryDate;

    @ManyToOne
    @JoinColumn(name="owner_email")
    private User owner;

    @ManyToOne
    @JoinColumn(name="workplace_id")
    private Workplace workplace;
    private int state;

    public long getId() {
        return id;
    }

    private static final Finder<String, Reservation> finder = new Finder<String, Reservation>(String.class, Reservation.class);

    public static List<Reservation> getAll(){
        return finder.all();
    }

    public static List<Reservation> getByUser(String email) {
        return finder.where().eq("owner.email", email).findList();
    }

    public static Reservation findById(long id) {
        return finder.where().eq("id", id).findUnique();
    }

   public static List<Reservation> findByDate(long workplaceId, Date startDate, Date endDate) {
       if (endDate == null) {
           return finder.where().eq("creationDate",startDate ).findList();
       }
       return finder.where().eq("workplace_id", workplaceId).where().between("creationDate", startDate, endDate).findList();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Workplace getWorkplace() {
        return workplace;
    }

    public void setWorkplace(Workplace workplace) {
        this.workplace = workplace;
    }

    public String getOfficeName() {
        return workplace.getOffice().getName();
    }

    public String getOfficeAddress() {
        Address address = workplace.getOffice().getAddress();
        return address.getCity() + ", " + address.getStreet();
    }

    public String getCreationDateForUser() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(creationDate);
    }

    public String getBeginningDateForUser() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(beginningDate);
    }

    public String getExpiryDateForUser() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(expiryDate);
    }

    public String getDurationForUser() {
        return String.valueOf(getDuration().toStandardDuration().getStandardDays());
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    /**
     * Zwraca czas trwania rezerwacji - różnica pomiędzy dniem dzisiejszym a datą utworzenia.
     * @return czas trwania rezerwacji.
     */
    public Days getDuration() {
        LocalDate endDate = new LocalDate(getExpiryDate());
        LocalDate currentDate = new LocalDate();
        return Days.daysBetween(currentDate, endDate);
    }

    public boolean inBounds(Reservation other) {
        LocalDate startDate = new LocalDate(getBeginningDate());
        LocalDate endDate = new LocalDate(getExpiryDate());
        LocalDate otherStartDate = new LocalDate(other.getBeginningDate());
        LocalDate otherEndDate = new LocalDate(other.getExpiryDate());

        if (getState() == BOOKED)
            return Days.daysBetween(startDate, otherStartDate).getDays() < MAX_DAYS_BOOKED;
        if (getState() == PROCESSING || getState() == BOOKED)
            return Days.daysBetween(startDate, otherStartDate).getDays() < MAX_DAYS_BOOKED;
        return true;
    }

    /**
     * Sprawdza czy rezerwacja wygasła.
     * @return true gdy rezerwacja wygasła, false w pzeciwnym wypadku.
     */
    public boolean isExpired() {
        int duration = getDuration().getDays();
        if (duration < 0)
            return true;

        return false;
    }
}
