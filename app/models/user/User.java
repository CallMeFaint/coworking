package models.user;

import play.db.ebean.*;

import javax.persistence.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="USERS")
public class User extends Model {
    private static final String PERMISSIONS_SEPARATOR = "|";

    @Id
    private String email;
    private String firstName;
    private String lastName;
    private String passwordHash;
    private String permissions;

    public User(String email, String password) {
        this.email = email;
        this.passwordHash = generateHash(password);
    }

    public User(String email, String password, String lastName, String firstName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.passwordHash = generateHash(password);
    }

    private static Finder<String, User> find = new Finder<String, User>(
            String.class, User.class
    );

    private String generateHash(String password) {
        try {
            MessageDigest cipher = MessageDigest.getInstance("MD5");
            cipher.update(password.getBytes());
            byte[] bytes = cipher.digest();
            StringBuilder output = new StringBuilder();

            for (int i = 0; i < bytes.length; i++)
                output.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));

            return output.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static User findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public List<Integer> getPermissionsAsList(){
        List<Integer> result = new ArrayList<Integer>();
        if (permissions == null)
            return result;

        if (permissions.contains(PERMISSIONS_SEPARATOR)) {
            String[] splittedPermissions = permissions.split("\\|");
            for (String p : splittedPermissions)
                result.add(Integer.valueOf(p));
        } else {
            result.add(Integer.valueOf(permissions.trim()));
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof User) {
            User candidate = (User) other;
            return email.equals(candidate.email) && passwordHash.equals(candidate.passwordHash);
        }
        return false;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
