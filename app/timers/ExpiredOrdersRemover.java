package timers;

import akka.actor.UntypedActor;
import models.office.Reservation;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import play.api.Play;

import java.util.List;

/**
 * Created by Damian on 2014-11-14.
 */
public class ExpiredOrdersRemover implements Runnable{

    @Override
    public void run() {
        List<Reservation> reservations = Reservation.getAll();
        for (Reservation reservation : reservations)
            if(reservation.isExpired())
                reservation.delete();
    }
}
