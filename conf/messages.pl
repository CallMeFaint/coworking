
login.error.user.invalid=Błędny adres e-mail lub hasło.

registration.error.passwords.not.the.same=Wpisane hasła różnią się.
registration.error.email.empty=Adres e-mail nie może być pusty.
registration.success=Rejestracja zakończyła się powodzeniem. Możesz się zalogować.
registration.error.user.exists=Użytkownik o podanym adresie e-mail już istnieje.

form.registration.label=Rejestracja:
form.registration.button=Zarejestruj się
form.login.label=Zaloguj się:
form.login.button=Zaloguj się
form.logut.button=Wyloguj się

enterpage.search.label="Wyszukaj biura z wykorzystaniem nazwy lub miasta".