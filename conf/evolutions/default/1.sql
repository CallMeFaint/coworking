# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table address (
  id                        bigint not null,
  city                      varchar(255),
  street                    varchar(255),
  constraint pk_address primary key (id))
;

create table office (
  id                        bigint not null,
  picture_path              varchar(255),
  name                      varchar(255),
  description               varchar(255),
  address_id                bigint,
  prize                     decimal(38),
  constraint uq_office_name unique (name),
  constraint pk_office primary key (id))
;

create table reservation (
  id                        bigint not null,
  creation_date             timestamp,
  expiry_date               timestamp,
  owner_email               varchar(255),
  workplace_id              bigint,
  state                     integer,
  constraint pk_reservation primary key (id))
;

create table USERS (
  email                     varchar(255) not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  password_hash             varchar(255),
  permissions               varchar(255),
  constraint pk_USERS primary key (email))
;

create table workplace (
  id                        bigint not null,
  office_id                 bigint,
  constraint pk_workplace primary key (id))
;

create sequence address_seq;

create sequence office_seq;

create sequence reservation_seq;

create sequence USERS_seq;

create sequence workplace_seq;

alter table office add constraint fk_office_address_1 foreign key (address_id) references address (id);
create index ix_office_address_1 on office (address_id);
alter table reservation add constraint fk_reservation_owner_2 foreign key (owner_email) references USERS (email);
create index ix_reservation_owner_2 on reservation (owner_email);
alter table reservation add constraint fk_reservation_workplace_3 foreign key (workplace_id) references workplace (id);
create index ix_reservation_workplace_3 on reservation (workplace_id);
alter table workplace add constraint fk_workplace_office_4 foreign key (office_id) references office (id);
create index ix_workplace_office_4 on workplace (office_id);



# --- !Downs

drop table if exists address cascade;

drop table if exists office cascade;

drop table if exists reservation cascade;

drop table if exists USERS cascade;

drop table if exists workplace cascade;

drop sequence if exists address_seq;

drop sequence if exists office_seq;

drop sequence if exists reservation_seq;

drop sequence if exists USERS_seq;

drop sequence if exists workplace_seq;

